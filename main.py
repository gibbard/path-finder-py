"""
Script to find the shortest distance between two nodes.

Should be run using python:

python main.py [data file] [start node] [end node]

@Author: Ben Gibbard <bgibbard@gmail.com>
"""

import argparse

from classes.Graph import Graph


def main():
    # Parse the command line params
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='The name of the node data file to parse')
    parser.add_argument('start', help='The start node')
    parser.add_argument('end', help='The end node')

    args = parser.parse_args()

    # Read the contents of the .dat file
    try:
        with open(args.file, 'r') as file:
            data = file.read()
    except FileNotFoundError:
        print('File ' + args.file + ' not found, aborting')
        exit(1)

    # Create Graph instance to store nodes and perform shortest path calculations
    graph = Graph(data)
    path = graph.shortest_path(args.start, args.end)

    print()
    if path['distance'] == Graph.INFINITY:
        print('There is no path from node "' + args.start + '" to node "' + args.end + '".')
    else:
        for node in path['path']:
            print(node)


if __name__ == '__main__':
    main()
