"""
Unit tests to verify the Graph path finding functionality.

@Author Ben Gibbard <bgibbard@gmail.com>
"""

import unittest

from classes.Graph import Graph


class GraphTestCase(unittest.TestCase):
    def test_bad_data(self):
        # Shouldn't throw an exception if just empty
        Graph('')

        # Shouldn't throw an exception if OK
        Graph('A B 10')
        Graph('A    B     10')
        Graph('A B 10\nB A 10')
        Graph('A B 10\nB A 10\n')
        Graph('startNode endNode distance\nA B 10\n')
        Graph('startNode    endNode     distance\nA B 10\n')

        # Otherwise bad data should cause system exit
        with self.assertRaises(SystemExit):
            Graph('A')
        with self.assertRaises(SystemExit):
            Graph('A B')
        with self.assertRaises(SystemExit):
            Graph('A B C')
        with self.assertRaises(SystemExit):
            Graph('A B 1 C')
        with self.assertRaises(SystemExit):
            Graph('A! B 1')
        with self.assertRaises(SystemExit):
            Graph('start Node    end Node     distance\nA B 10\n')
        with self.assertRaises(SystemExit):
            Graph('A B 10\nstartNode endNode distance\n')

    def test_two_nodes_bidirectional(self):
        graph = Graph('A B 10\nB A 10')

        path = graph.shortest_path('A', 'B')
        self.assertEqual(path['distance'], 10)
        self.assertEqual(path['path'], ['A', 'B'])

        path = graph.shortest_path('B', 'A')
        self.assertEqual(path['distance'], 10)
        self.assertEqual(path['path'], ['B', 'A'])

    def test_two_nodes_unidirectional(self):
        graph = Graph('A B 10')

        path = graph.shortest_path('A', 'B')
        self.assertEqual(path['distance'], 10)
        self.assertEqual(path['path'], ['A', 'B'])

        path = graph.shortest_path('B', 'A')
        self.assertEqual(path['distance'], Graph.INFINITY)
        self.assertEqual(path['path'], [])

    def test_three_nodes_bidirectional_single_path(self):
        graph = Graph('A B 10\nB A 10\nB C 20\nC B 20')

        path = graph.shortest_path('A', 'B')
        self.assertEqual(path['distance'], 10)
        self.assertEqual(path['path'], ['A', 'B'])
        path = graph.shortest_path('B', 'C')
        self.assertEqual(path['distance'], 20)
        self.assertEqual(path['path'], ['B', 'C'])
        path = graph.shortest_path('A', 'C')
        self.assertEqual(path['distance'], 30)
        self.assertEqual(path['path'], ['A', 'B', 'C'])

        path = graph.shortest_path('C', 'A')
        self.assertEqual(path['distance'], 30)
        self.assertEqual(path['path'], ['C', 'B', 'A'])
        path = graph.shortest_path('C', 'B')
        self.assertEqual(path['distance'], 20)
        self.assertEqual(path['path'], ['C', 'B'])
        path = graph.shortest_path('B', 'A')
        self.assertEqual(path['distance'], 10)
        self.assertEqual(path['path'], ['B', 'A'])

    def test_three_nodes_unidirectional_single_path(self):
        graph = Graph('A B 10\nB C 20')

        path = graph.shortest_path('A', 'B')
        self.assertEqual(path['distance'], 10)
        path = graph.shortest_path('B', 'C')
        self.assertEqual(path['distance'], 20)
        path = graph.shortest_path('A', 'C')
        self.assertEqual(path['distance'], 30)

        path = graph.shortest_path('C', 'A')
        self.assertEqual(path['distance'], Graph.INFINITY)
        self.assertEqual(path['path'], [])
        path = graph.shortest_path('C', 'B')
        self.assertEqual(path['distance'], Graph.INFINITY)
        self.assertEqual(path['path'], [])
        path = graph.shortest_path('B', 'A')
        self.assertEqual(path['distance'], Graph.INFINITY)
        self.assertEqual(path['path'], [])

    def test_three_nodes_split_path(self):
        # Test single step being either shorter or longer than double step, ensure path chosen correct for either
        # condition
        for distance in range(1, 40):
            graph = Graph('A B 10\nB A 10\nB C 20\nC B 20\nA C ' + str(distance) + '\nC A ' + str(distance))
            path = graph.shortest_path('A', 'C')
            if distance < 30:
                self.assertEqual(path['distance'], distance)
                self.assertEqual(path['path'], ['A', 'C'])
            elif distance == 30:
                self.assertEqual(path['distance'], distance)
            else:
                self.assertEqual(path['distance'], 30)
                self.assertEqual(path['path'], ['A', 'B', 'C'])

    def test_two_paths_choice_returns_shortest(self):
        # Given a choice of two routes, ensure we are always given the shortest
        # Route is A->B->D or A->C->D
        # "Dead-end" of B->X should have no impact
        for distance_a_b in range(1, 10):
            for distance_b_d in range(1, 10):
                for distance_b_x in range(1, 10):
                    for distance_a_c in range(1, 10):
                        for distance_c_d in range(1, 10):
                            graph = Graph(
                                'A B ' + str(distance_a_b) + '\n'
                                + 'B A ' + str(distance_a_b) + '\n'
                                + 'B D ' + str(distance_b_d) + '\n'
                                + 'D B ' + str(distance_b_d) + '\n'
                                + 'B X ' + str(distance_b_x) + '\n'
                                + 'X B ' + str(distance_b_x) + '\n'
                                + 'A C ' + str(distance_a_c) + '\n'
                                + 'C A ' + str(distance_a_c) + '\n'
                                + 'C D ' + str(distance_c_d) + '\n'
                                + 'D C ' + str(distance_c_d)
                            )

                            # A -> D
                            path = graph.shortest_path('A', 'D')
                            a_b_d = distance_a_b + distance_b_d
                            a_c_d = distance_a_c + distance_c_d
                            if a_b_d < a_c_d:
                                self.assertEqual(path['distance'], a_b_d)
                                self.assertEqual(path['path'], ['A', 'B', 'D'])
                            elif a_c_d < a_b_d:
                                self.assertEqual(path['distance'], a_c_d)
                                self.assertEqual(path['path'], ['A', 'C', 'D'])
                            else:
                                self.assertEqual(path['distance'], a_c_d)

                            # And how about A -> B (but should go A -> C -> D -> B when shorter)
                            path = graph.shortest_path('A', 'B')
                            a_b = distance_a_b
                            a_c_d_b = distance_a_c + distance_c_d + distance_b_d
                            if a_b < a_c_d_b:
                                self.assertEqual(path['distance'], a_b)
                                self.assertEqual(path['path'], ['A', 'B'])
                            elif a_c_d_b < a_b:
                                self.assertEqual(path['distance'], a_c_d_b)
                                self.assertEqual(path['path'], ['A', 'C', 'D', 'B'])
                            else:
                                self.assertEqual(path['distance'], a_c_d_b)


if __name__ == '__main__':
    unittest.main()
