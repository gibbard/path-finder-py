
# path-finder-py

Script to find the shortest route between two nodes Uses Dijkstra's algorithm.
 * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

# Fetching
Clone the latest version of this project:
```
> git clone https://gitlab.com/gibbard/path-finder-py
```

# Using
This script can be executed using python3 within the path-finder-py folder:
````
> python main.py [data file] [start node] [end node]
````

The output will be a line-separated list of nodes that represent the shortest
path between [start node] and [end node], or a warning if no path can be found.

For example - A valid path:
```
> python main.py exmouth-links.dat J1053 J1037

J1053
J1035
J1036
J1037
```
No path:
```
> python main.py exmouth-links.dat J1001 J1023

There is no path from node "J1001" to node "J1023".
```

# Testing
Unit testing can be performed by running:
```
> python test.py
```
within the path-finder-py folder. No sample data is required for unit testing.

Unit testing has been verified on Python 3.6.9 (Ubuntu) and 3.8.3 (Windows).

# Sample data
A sample road network (exmouth-links.dat) is provided here:

https://gist.github.com/byrney/a64f62850d1c5f5ca287ce14dec5f7ad

The format of the file is

```
startNode  endNode  distance
A          B        2
B          C        4
```
