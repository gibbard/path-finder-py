"""
Graph class to store a graph's nodes, and provide functionality for finding the shortest
path between any two nodes.

Uses Dijkstra's algorithm
https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

@Author Ben Gibbard <bgibbard@gmail.com>
"""

import re

from classes.Node import Node


class Graph:
    INFINITY = -1

    def __init__(self, data: str):
        """
        Create instance of Graph with the supplied data

        :param data:    The contents of the .dat file containing the node information
        """
        self.nodes = {}
        self.distances = None
        self.visited = None

        self.__parse_data(data)

    def __parse_data(self, data: str):
        """
        Parse the node data from the .dat file which has been loaded.
        For each row ensure that the node data is valid.
        :param data:
        :return:
        """

        # Regular expressions to validate a node row or the header (if supplied)
        regex_header = re.compile(r"^startNode\s+endNode\s+distance$", re.IGNORECASE)
        regex_node = re.compile(r"^([0-9A-Z]+)\s+([0-9A-Z]+)\s+([0-9]+)$", re.IGNORECASE)

        lines = data.split('\n')

        for row, node_data in enumerate(lines):
            if node_data == '':
                continue

            # Node row ?
            node_match = regex_node.search(node_data)
            if node_match:
                node = self.__get_node(node_match.group(1))
                node.add_link(node_match.group(2), int(node_match.group(3)))

            else:
                # Header row?
                header_match = regex_header.search(node_data)
                if not header_match or row > 0:
                    print('Row "' + node_data + '" is incorrectly formatted, aborting.')
                    exit(1)

    def __get_node(self, name: str) -> Node:
        """
        Get node from the graph matching name; Create if it doesn't already exist.

        :param name:    The name of the node to fetch.
        :return:        The node matching the supplied name.
        """
        if name not in self.nodes:
            self.nodes[name] = Node(name)

        return self.nodes[name]

    def __get_next_node(self) -> str:
        """
        Get the node that has the shortest overall distance from the start node, and that we have not visited yet.
        This is the next node to search from.

        :return:            Name of the node with the shortest distance from the start node, which we have not yet visited.
        """
        shortest_node = ''
        shortest_distance = Graph.INFINITY

        for node, distance in self.distances.items():
            if distance == Graph.INFINITY:
                continue

            if (shortest_node == '' or distance < shortest_distance) and node not in self.visited:
                shortest_node = node
                shortest_distance = distance

        return shortest_node

    def shortest_path(self, start: str, end: str) -> dict:
        """
        Uses Dijkstras path finding algorithm, ideal when no heuristics to tune the A-Star algorithm.
        https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

        :param start:   The name of the start node.
        :param end:     The name of the end node.
        :return:        dict {path: [list of node names to get from the start node to the end node in the shortest
                        distance], distance: [the distance for the shortest path]}.
        """
        # Record the overall distance to a node from the start node that we have discovered so far.
        self.distances = {end: Graph.INFINITY}

        # Record visited nodes
        self.visited = []

        # Record the parent node for a child node as we create a path
        parents = {end: ''}

        # Populate distances & set parent for nodes linked from the starting node
        if start in self.nodes:
            for child, distance in self.nodes[start].link_distances.items():
                self.distances[child] = distance
                parents[child] = start

        # Find the current node with shortest overall distance from the start node so far
        node = self.__get_next_node()

        # And keep traversing from that node to child nodes until we either find the end node, or
        # discover that there is no path from start to end.
        while node != '':
            # Distance from the start node so far for this current node
            start_distance = self.distances[node]

            # Process the child nodes for this current node
            if node in self.nodes:
                for child, distance in self.nodes[node].link_distances.items():
                    # Skip if this is the start node
                    if child == start:
                        continue

                    # Save the distance from the start node to this child node
                    child_distance = start_distance + distance

                    # If there's no distance from the start node to this child node,
                    # or if the distance is shorter than the existing distance from
                    # the start node to the child node, then update known distances
                    # and set the parent node
                    if child not in self.distances or self.distances[child] > child_distance or self.distances[child] == Graph.INFINITY:
                        # Distance to this child node from the start node
                        self.distances[child] = child_distance

                        # Add to parents so that we can determine our shortest path later
                        parents[child] = node

            # Record the current node in the visited array so that we don't re-visit later
            self.visited.append(node)

            # Find the next node that has the shortest overall distance from the start
            node = self.__get_next_node()

        # We have either reached the end node, or no route from start -> end could be found
        shortest_path = []
        if self.distances[end] != Graph.INFINITY:
            # We have found a route. Populate the shortest path array
            shortest_path.append(end)
            parent = parents[end]
            while parent != start:
                shortest_path.append(parent)
                parent = parents[parent]

            # Add the start node
            shortest_path.append(start)

            # Reverse path so that it is from start -> end
            shortest_path.reverse()

        # Return the shortest path & the end node's distance from the start node
        return {
            'distance': self.distances[end],
            'path': shortest_path
        }
