"""
Node class to store information on a node and the other nodes that it is linked to.

@Author Ben Gibbard <bgibbard@gmail.com>
"""


class Node:
    def __init__(self, name):
        self.name = name
        self.link_distances = {}

    def add_link(self, node: str, distance: int):
        """
        Add a link between this node and another, recording the link distance

        :param node:        The name of the node to create a link to
        :param distance:    The distance of the link
        """
        self.link_distances[node] = distance
